# README #

Progetto di Data Intensive 2021 
Bernardini Yuri 
Matricola 830601
## Fonti
Link database Kaggle.com : https://www.kaggle.com/fedesoriano/stroke-prediction-dataset
## Files
### Progetto sviluppato su Colab: 
- link al notebook: https://colab.research.google.com/drive/1kbezSr3nZIsE6a9owh1npwYjMN7Bejgd?usp=sharing
- A disposizione anche file pdf, html, e .ipynb
